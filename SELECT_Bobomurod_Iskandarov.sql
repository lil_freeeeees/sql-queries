WITH staff_revenue AS (
    SELECT staff_id, SUM(amount) as total_revenue
    FROM payment
    WHERE EXTRACT(YEAR FROM payment_date) = 2017
    GROUP BY staff_id
)
SELECT s.staff_id, s.first_name, s.last_name, s.store_id, sr.total_revenue
FROM staff_revenue sr
INNER JOIN staff s ON sr.staff_id = s.staff_id
ORDER BY s.store_id, sr.total_revenue DESC;

SELECT 
    s.staff_id, 
    s.first_name, 
    s.last_name, 
    s.store_id, 
    SUM(p.amount) as total_revenue
FROM 
    staff s
INNER JOIN 
    payment p ON s.staff_id = p.staff_id
WHERE 
    EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY 
    s.staff_id, 
    s.first_name, 
    s.last_name, 
    s.store_id
ORDER BY 
    s.store_id,
    total_revenue DESC;

   
SELECT 
 f.title, 
 COUNT(r.rental_id) as rental_count
FROM 
    rental r
INNER JOIN 
    inventory i ON r.inventory_id = i.inventory_id
INNER JOIN 
    film f ON i.film_id = f.film_id
GROUP BY 
    f.title
ORDER BY 
    rental_count DESC
LIMIT
    5;

SELECT 
    title, 
    (SELECT COUNT(*) FROM rental r 
     JOIN inventory i ON r.inventory_id = i.inventory_id 
     WHERE i.film_id = f.film_id) AS rental_count 
FROM 
    film f 
ORDER BY 
    rental_count DESC 
LIMIT 
    5;
    
   SELECT actor_id, first_name, last_name, MAX(gap) AS longest_gap
FROM (
    SELECT actor_id, first_name, last_name, film_id, release_year - LAG(release_year, 1) OVER (PARTITION BY actor_id ORDER BY release_year) AS gap
    FROM (
        SELECT a.actor_id, a.first_name, a.last_name, f.film_id, f.release_year
        FROM actor a
        JOIN film_actor fa ON a.actor_id = fa.actor_id
        JOIN film f ON fa.film_id = f.film_id
    )
)
GROUP BY actor_id, first_name, last_name
ORDER BY longest_gap DESC;

SELECT actor_id, first_name, last_name, MAX(gap) AS longest_gap
FROM (
    SELECT 
        a.actor_id, a.first_name, a.last_name, f.film_id, f.release_year,
        COALESCE(f.release_year - LAG(f.release_year) OVER (PARTITION BY a.actor_id ORDER BY f.release_year), 0) AS gap
    FROM actor a
    JOIN film_actor fa ON a.actor_id = fa.actor_id
    JOIN film f ON fa.film_id = f.film_id
) AS actor_film_data
GROUP BY actor_id, first_name, last_name
ORDER BY longest_gap DESC;
